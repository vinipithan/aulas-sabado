// Cria a lista
var lista = []

lista.push( 0 )
lista.push( 1 )
lista.push( 2 )
lista.push( 3 )
lista.push( 4 )
lista.push( 5 )
lista.push( 6 )
lista.push( 7 )
lista.push( 8 )
lista.push( 9 )
lista.push( 10 )

// Usando o comando Array.filter os numeros menores de 5
function menorQueCinco(numero) {
    if ( numero < 5 ) {
        return true;
    }
    return false;
}
console.log(lista.filter(menorQueCinco))

// Usando o comando Array.map multiplique a lista por 3
function multiploPorTres(multiplo) {
    return multiplo * 3
}
console.log(lista.map(multiploPorTres))

// Usando o comando Array.find Localize o numero 5 na lista
function localizaQuinto(cinco) {
    return cinco === 5
}
console.log(lista.find(localizaQuinto))

