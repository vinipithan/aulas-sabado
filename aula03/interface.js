var Gamer = (function () {
    function Gamer() {
        this.ligado = false;
        this.softwares = [];
        this.ip = "";
    }
    Gamer.prototype.ligar = function () {
        if (this.ligado) {
            throw "Computador já está ligado";
        }
        this.ligado = true;
        console.log("Computador ligado");
    };
    Gamer.prototype.desligar = function () {
        if (this.ligado) {
            this.ligado = false;
            console.log("Seu computador foi desliado");
        }
    };
    Gamer.prototype.instalar = function (sistemaOperacional) {
        this.softwares.push(sistemaOperacional);
    };
    return Gamer;
})();
var Laptop = (function () {
    function Laptop() {
        this.ligado = false;
        this.bateria = false;
        this.softwares = [];
        this.ip = "";
    }
    Laptop.prototype.ligar = function () {
        if (!this.ligado) {
            console.log("Computador ligado");
            this.ligado = true;
            return;
        }
        if (!this.bateria) {
            console.log("Bateria carregada");
            this.bateria = true;
            return;
        }
        throw "Recarregar a bateria";
    };
    Laptop.prototype.desligar = function () {
        if (this.ligado) {
            this.ligado = false;
            console.log("Seu computador foi desliado");
        }
    };
    Laptop.prototype.instalar = function (sistemaOperacional) {
        this.softwares.push(sistemaOperacional);
    };
    return Laptop;
})();
var computador = new Gamer();
//computador.ligar()
//computador.desligar()
computador.instalar("Windows");
computador.instalar("Urban Terror 4.03.2");
computador.ip = "10.10.1.33";
var laptop = new Laptop();
//laptop.ligar()
//laptop.desligar()
laptop.instalar("Linux");
laptop.instalar("Visual Studio Code");
computador.ip = "10.10.1.202";
//console.log(computador.softwares)
//console.log(laptop.softwares)
var computadorDavid = new Gamer();
computadorDavid.instalar("Linux");
computadorDavid.instalar("Eclipse");
computadorDavid.instalar("Visual Studio");
computadorDavid.ip = "10.10.1.191";
var computadorSouza = new Gamer();
computadorSouza.instalar("Linux");
computadorSouza.ip = "10.10.1.29";
var computadorRafael = new Laptop();
computadorRafael.instalar("Windows");
computadorRafael.instalar("Eclipse");
computadorRafael.ip = "10.10.1.18";
var listaDSV = [];
listaDSV.push(computadorDavid);
listaDSV.push(computadorSouza);
listaDSV.push(computadorRafael);
console.log(listaDSV);
