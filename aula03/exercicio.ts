interface Organico {
    alimentar: string[]
};

interface Vegetal {
    tipoAlimento: (Decompositores: string) => void
};

interface Inseto {
    tipoAlimento: (Vegetal: string) => void
};

interface Anfibio {
    tipoAlimento: (Inseto: string) => void
};

interface Decompositores {
    tipoAlimento: (Vegetal: string, Inseto: string, Anfibio: string) => void
};

class Planta implements Vegetal, Organico {
    alimentar: string[] = []
    tipoAlimento (Decompositores: string) {
        this.alimentar.push(Decompositores)
    }
};

class Gafanhoto implements Inseto, Organico {
    alimentar: string[] = []
    tipoAlimento (Vegetal: string) {
        this.alimentar.push(Vegetal)
    }
};

class Sapo implements Anfibio, Organico {
    static forEach: any;
    alimentar: string[] = []
    tipoAlimento (Inseto: string) {
        this.alimentar.push(Inseto)
    }
};

class Fungos implements Decompositores, Organico {
    alimentar: string[] = []
    tipoAlimento (Vegetal: string, Inseto: string, Anfibio: string) {
        this.alimentar.push(Vegetal, Inseto, Anfibio)
    }
};

var samambaia: Vegetal = new Planta()
samambaia.tipoAlimento("Adubo")

var louvaDeus: Inseto = new Gafanhoto()
louvaDeus.tipoAlimento("Folhas")

var slippy: Anfibio = new Sapo()
slippy.tipoAlimento("Moscas")

var decomposicao: Decompositores[] = []
decomposicao.push(samambaia)
decomposicao.push(louvaDeus)
decomposicao.push(slippy)

decomposicao.forEach(function (alimento) {
    console.log(alimento)
});