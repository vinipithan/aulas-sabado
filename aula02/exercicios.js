// @ts-check
/*
- Crie um método que some dois argumentos respeitando a seguinte tipagem:
type somaType = ( parameter1:number, parameter2:number ) => number
*/
function soma( parameter1, parameter2 ) {
    return parameter1 + parameter2;
};

/*- Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.
*/
var lista = [ "lista de palavras" ];
var caracteres = 0;
for (var i = 0; i < lista.length; i++) {
    caracteres = lista[i].length;
};
var quantidade = [];
quantidade.push(caracteres);

/*- Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, 
de intervalo entre duas datas.
type intervaloDiasType = ( parameter1:number, parameter2:number ) => string
*/
var nasc=new Date(1977, 11, 9, 14, 25, 0);
var hoje=new Date(2018, 4, 26, 9, 30, 0);
/*if (nasc.getMonth()==11 && nasc.getDate()>25) 
{
cmas.setFullYear(cmas.getFullYear()+1); 
}*/
var year=((1000*60*60*24) * 365) + (((1000 * 60 * 60 * 24) / (1000 * 60 * 60)) * 6);
var day=1000*60*60*24;
var hour=(1000 * 60 * 60 * 24) / (1000 * 60 * 60);
var minute=(1000 * 60 * 60) / (1000 * 60);
var second=(1000 * 60) / 1000;

var getAno = Math.floor((hoje.getTime()-nasc.getTime())/(year));
var getDia = Math.floor((hoje.getTime()-nasc.getTime())/(day));
var getHora = Math.floor((hoje.getTime()-nasc.getTime())/(hour));
var getMinuto = Math.floor((hoje.getMinutes()-nasc.getMinutes())/(minute));
var getSegundo = Math.floor((hoje.getSeconds()-nasc.getSeconds())/(second));

console.log(getAno + " anos", getDia + " dias", getHora + " horas", getMinuto  + " minutos", getSegundo + " segundos");
//40 anos, 5 meses, 17 dias
/*
var diferenca = new Date();
console.log(data2.getTime()); //date to timestamp
console.log(new Date(253203913000)); //timestamp to date*/


/*- Crie um método chamado calculadora que receberá uma lista de argumentos + função callback 
que fara o cálculo desejado.
type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number
*/
